import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient) { }

  private static TRANSACTIONS_URL = '/api/1.0/bank-service/user/account/transaction';

  async fetchTransactions() {
    try {
      const data: any = await this.http.get(TransactionService.TRANSACTIONS_URL).toPromise();
      return data;
    } catch (error) {
      console.error(`Error occurred: ${error}`);
    }
  }
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ TransactionService ],
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  panelOpenState = false;

  transactions: any[] = [];

  constructor(private transactionService: TransactionService){ }
  
  ngOnInit(): void {
    this.transactionService.fetchTransactions().then( data => {
      this.transactions = data;
    });
  }
}
