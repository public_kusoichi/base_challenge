package com.bank.bankservice.domains;

public enum TransactionType {
	
	CREDIT, DEBIT
}
