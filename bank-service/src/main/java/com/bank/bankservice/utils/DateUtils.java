package com.bank.bankservice.utils;

import java.sql.Timestamp;
import java.time.Instant;

public final class DateUtils {

	public static String getCurrentTimestamp() {
		return Timestamp.from(Instant.now()).toString();
	}
}
