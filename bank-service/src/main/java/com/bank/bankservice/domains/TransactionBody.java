package com.bank.bankservice.domains;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class TransactionBody {
	
	@NotNull
	private TransactionType transacctionType;
	@NotNull
	private BigDecimal amount;
	
	public TransactionType getTransacctionType() {
		return transacctionType;
	}
	public void setTransacctionType(TransactionType transacctionType) {
		this.transacctionType = transacctionType;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
