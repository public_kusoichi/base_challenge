package com.bank.bankservice.exceptions;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = BAD_REQUEST, code = BAD_REQUEST, reason = "Invalid amount.")
public class NotValidAmountException extends RuntimeException {
	private static final long serialVersionUID = 8604868714272219783L;
	public NotValidAmountException(String message) {
		super(message);
	}
}
