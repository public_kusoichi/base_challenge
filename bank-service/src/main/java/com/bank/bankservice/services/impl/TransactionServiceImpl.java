package com.bank.bankservice.services.impl;

import static com.bank.bankservice.transformers.DtoTransformer.copyTransaction;
import static com.bank.bankservice.utils.DateUtils.getCurrentTimestamp;
import static java.math.BigDecimal.ZERO;
import static java.util.Objects.isNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.bank.bankservice.builder.GenericBuilder;
import com.bank.bankservice.domains.Balance;
import com.bank.bankservice.domains.Transaction;
import com.bank.bankservice.domains.TransactionBody;
import com.bank.bankservice.domains.TransactionType;
import com.bank.bankservice.exceptions.NotValidAmountException;
import com.bank.bankservice.exceptions.TransactionNotFoundException;
import com.bank.bankservice.services.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {

	private final static List<Transaction> TRANSACTIONS = new ArrayList<>();
	
	@Override
	public List<Transaction> getAllTransactions() {
		
		return Collections.unmodifiableList(TRANSACTIONS);
	}

	@Override
	public Transaction addTransaction(TransactionBody tb) {
		
		BigDecimal amount = tb.getAmount();
		if (amount.compareTo(ZERO) <= 0)
			throw new NotValidAmountException(String
					.format("Invalid amount: %s", amount.toString()));

		String id = UUID.randomUUID().toString();
		TransactionType type = tb.getTransacctionType();
		String effectiveDate = getCurrentTimestamp();
		
		Transaction transaction = GenericBuilder.of(Transaction::new)
				.with(Transaction::setId, id)
				.with(Transaction::setType, type)
				.with(Transaction::setAmount, amount)
				.with(Transaction::setEffectiveDate, effectiveDate)
				.build();
		
		TRANSACTIONS.add(transaction);
		
		Transaction copy = copyTransaction(transaction);
		
		return copy;
	}

	@Override
	public Transaction getAllTransactionById(String id) {
		
		Transaction source = TRANSACTIONS.stream()
				.filter(t -> id.equalsIgnoreCase(t.getId()))
				.findAny()
				.orElse(null);
		
		if (isNull(source))
			throw new TransactionNotFoundException(String.format("Transaction %s not found.", id));
		
		Transaction copy = copyTransaction(source);
		
		return copy;
	}

	@Override
	public Balance getBalance() {
		
		BigDecimal totalDebit = TRANSACTIONS.stream()
				.filter(e -> e.getType() == TransactionType.DEBIT)
				.map(e -> e.getAmount())
				.reduce(ZERO, BigDecimal::add);
		
		BigDecimal totalCredit = TRANSACTIONS.stream()
				.filter(e -> e.getType() == TransactionType.CREDIT)
				.map(e -> e.getAmount())
				.reduce(ZERO, BigDecimal::add);
		
		BigDecimal balance = totalCredit.subtract(totalDebit);
		
		return GenericBuilder.of(Balance::new)
				.with(Balance::setBalance, balance)
				.with(Balance::setVersion, getCurrentTimestamp())
				.build();
	}
}


















