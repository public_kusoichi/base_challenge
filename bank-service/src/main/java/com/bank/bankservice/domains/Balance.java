package com.bank.bankservice.domains;

import java.math.BigDecimal;

public class Balance {

	private BigDecimal balance;
	private String version;
	
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	

}
