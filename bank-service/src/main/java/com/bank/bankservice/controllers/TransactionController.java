package com.bank.bankservice.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bank.bankservice.domains.Balance;
import com.bank.bankservice.domains.Transaction;
import com.bank.bankservice.domains.TransactionBody;
import com.bank.bankservice.services.TransactionService;

@RestController
@RequestMapping("/api/1.0/bank-service")
public class TransactionController {
	
	@Autowired
	private TransactionService service;
	
	@GetMapping("/user/account/transaction")
	public List<Transaction> getAll() {
		return service.getAllTransactions();
	}
	
	@PostMapping("/user/account/transaction")
	public Transaction createTransacction(@Valid @RequestBody TransactionBody transactionBody) {
		return service.addTransaction(transactionBody);
	}
	
	@GetMapping("/user/account/transaction/{id}")
	public Transaction getById(@PathVariable("id") String id) {
		return service.getAllTransactionById(id);		
	}
	
	@GetMapping("/user/account/balance")
	public Balance getBalance() {
		return service.getBalance();
	}
}
