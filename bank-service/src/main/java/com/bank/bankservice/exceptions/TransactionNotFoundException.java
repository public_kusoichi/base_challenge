package com.bank.bankservice.exceptions;

import static org.springframework.http.HttpStatus.NOT_FOUND;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = NOT_FOUND, code = NOT_FOUND, reason = "No such a transaction.")
public class TransactionNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1209463405177576382L;
	public TransactionNotFoundException(String message) {
		super(message);
	}
}
