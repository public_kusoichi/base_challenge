package com.bank.bankservice.services;

import java.util.List;

import com.bank.bankservice.domains.Balance;
import com.bank.bankservice.domains.Transaction;
import com.bank.bankservice.domains.TransactionBody;

public interface TransactionService {

	List<Transaction> getAllTransactions();

	Transaction addTransaction(TransactionBody tb);

	Transaction getAllTransactionById(String id);

	Balance getBalance();
}
