package com.bank.bankservice.transformers;

import java.util.Objects;

import com.bank.bankservice.builder.GenericBuilder;
import com.bank.bankservice.domains.Transaction;

public final class DtoTransformer {

	public static Transaction copyTransaction(Transaction source) {
		if (Objects.isNull(source))
			return null;
		return GenericBuilder.of(Transaction::new)
				.with(Transaction::setId, source.getId())
				.with(Transaction::setType, source.getType())
				.with(Transaction::setAmount, source.getAmount())
				.with(Transaction::setEffectiveDate, source.getEffectiveDate())
				.build();
	}
}
